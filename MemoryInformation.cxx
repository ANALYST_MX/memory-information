#include "MemoryInformation.hxx"

MemoryInformation::MemoryInformation(const size_t scale)
  : scale(scale)
{
}

MemoryInformation::~MemoryInformation()
{
}

void MemoryInformation::setScale(const size_t newScale)
{
  scale = newScale;
}

size_t MemoryInformation::getScale() const
{
  return scale;
}

std::ostream& operator<<(std::ostream &os, const MemoryInformation &mi)
{
  os << "         " << std::setw(15) << "total" << std::setw(15) << "used" << std::setw(15) << "free"
     << std::setw(15) << "active" << std::setw(15) << "inactive" << std::setw(15) << "biffered"
     << std::setw(15) << "cached" << std::setw(15) << "locked" << std::setw(15) << "available" << std::endl;
  os << "Memory:  " << std::setw(15) << mi.getVirtualMemory() << std::setw(15) << mi.getUsedMemory()
     << std::setw(15) << mi.getFreeMemory() << std::setw(15) << mi.getActiveMemory()
     << std::setw(15) << mi.getInactiveMemory() << std::setw(15) << mi.getBufferedMemory()
     << std::setw(15) << mi.getCachedMemory() << std::setw(15) << mi.getLockedMemory()
     << std::setw(15) << mi.getAvailableMemory() << std::endl;
  os << "Swap:    " << std::setw(15) << mi.getSwapMemory() << std::setw(15) << mi.getUsedSwapMemory()
     << std::setw(15) << mi.getFreeSwapMemory();
  return os;
}

size_t MemoryInformation::getPhysicalMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return memInfo.totalram * memInfo.mem_unit / scale;
}

size_t MemoryInformation::getVirtualMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return memInfo.totalram * memInfo.mem_unit / scale;
}

size_t MemoryInformation::getPageSize() const
{
  return sysconf(_SC_PAGE_SIZE) / scale;
}

size_t MemoryInformation::getPhysicalPages() const
{
  return sysconf(_SC_PHYS_PAGES) / scale;
}

size_t MemoryInformation::getFreeMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return memInfo.freeram * memInfo.mem_unit / scale;
}

size_t MemoryInformation::getUsedMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return (memInfo.totalram - memInfo.freeram) * memInfo.mem_unit / scale;
}

size_t MemoryInformation::getAvailableMemory() const
{
  size_t mavailable;
  std::string dummy;
  std::string identifier = "MemAvailable:";
  std::string strLine = getMatchedString("/proc/meminfo", identifier);
  std::istringstream iss(strLine);
  iss.ignore(identifier.length());
  iss >> mavailable;
  iss >> dummy;
  return mavailable * getMultiplier(dummy) / scale;
}

size_t MemoryInformation::getBufferedMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return memInfo.bufferram * memInfo.mem_unit / scale;
}

size_t MemoryInformation::getCachedMemory() const
{
  size_t mcached;
  std::string dummy;
  std::string identifier = "Cached:";
  std::string strLine = getMatchedString("/proc/meminfo", identifier);
  std::istringstream iss(strLine);
  iss.ignore(identifier.length());
  iss >> mcached;
  iss >> dummy;
  return mcached * getMultiplier(dummy) / scale;
}

size_t MemoryInformation::getActiveMemory() const
{
  size_t mactive;
  std::string dummy;
  std::string identifier = "Active:";
  std::string strLine = getMatchedString("/proc/meminfo", identifier);
  std::istringstream iss(strLine);
  iss.ignore(identifier.length());
  iss >> mactive;
  iss >> dummy;
  return mactive * getMultiplier(dummy) / scale;
}

size_t MemoryInformation::getInactiveMemory() const
{
  size_t minactive;
  std::string dummy;
  std::string identifier = "Inactive:";
  std::string strLine = getMatchedString("/proc/meminfo", identifier);
  std::istringstream iss(strLine);
  iss.ignore(identifier.length());
  iss >> minactive;
  iss >> dummy;
  return minactive * getMultiplier(dummy) / scale;
}

size_t MemoryInformation::getLockedMemory() const
{
  size_t mlocked;
  std::string dummy;
  std::string identifier = "Mlocked:";
  std::string strLine = getMatchedString("/proc/meminfo", identifier);
  std::istringstream iss(strLine);
  iss.ignore(identifier.length());
  iss >> mlocked;
  iss >> dummy;
  return mlocked * getMultiplier(dummy) / scale;
}

size_t MemoryInformation::getSwapMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return memInfo.totalswap * memInfo.mem_unit / scale;
}

size_t MemoryInformation::getFreeSwapMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return memInfo.freeswap * memInfo.mem_unit / scale;
}

size_t MemoryInformation::getUsedSwapMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return (memInfo.totalswap - memInfo.freeswap) * memInfo.mem_unit / scale;
}

size_t MemoryInformation::getSharedMemory() const
{
  struct sysinfo memInfo;
  sysinfo(&memInfo);
  return memInfo.sharedram * memInfo.mem_unit / scale;
}

std::string MemoryInformation::getMatchedString(const std::string &path, const std::string &identifier) const
{
  std::ifstream fin;
  std::string dummy;
  fin.open(path, std::ifstream::in);
  if ( fin )
    {
      while ( !fin.eof() )
	{
	  std::getline(fin, dummy);
	  if ( dummy.find(identifier) != std::string::npos )
	    {
	      return dummy;
	    }
	}
    }

  return "";
}

size_t MemoryInformation::getMultiplier(std::string &str) const
{
  if ( compareCaseInsensitive(str, "kb") || compareCaseInsensitive(str, "kib") )
    {
      return BinaryPrefix::KB;
    }
  else if ( compareCaseInsensitive(str, "mb") || compareCaseInsensitive(str, "mib") )
    {
      return BinaryPrefix::MB;
    }
  else if ( compareCaseInsensitive(str, "gb") || compareCaseInsensitive(str, "gib") )
    {
      return BinaryPrefix::GB;
    }
  else if ( compareCaseInsensitive(str, "tb") || compareCaseInsensitive(str, "tib") )
    {
      return BinaryPrefix::TB;
    }
  else
    {
      return 0;
    }
}

bool MemoryInformation::compareCaseInsensitive(std::string strFirst, std::string strSecond) const
{
  transform(strFirst.begin(), strFirst.end(), strFirst.begin(), toupper);
  transform(strSecond.begin(), strSecond.end(), strSecond.begin(), toupper);
  if ( strFirst == strSecond )
    {
      return true;
    }
  
  return false;
}
