#include "main.hxx"

int main(int argc, char *argv[])
{
  int ch;
  int factor = BinaryPrefix::KB;
  int vflag = 0;
  
  opterr = 0; // выключим вывод сообщений об ошибке
  while ( (ch = getopt(argc, argv, "bghkmv")) != EOF )
    {
      switch ( ch )
	{
	case 'b':
	  factor = 1;
	  break;
	case 'g':
	  factor = BinaryPrefix::GB;
	  break;
	case 'h':
	  usage();
	  exit(EXIT_SUCCESS);
	case 'k':
	  factor = BinaryPrefix::KB;
	  break;
	case 'm':
	  factor = BinaryPrefix::MB;
	  break;
	case 'v':
	  vflag = 1;
	  break;
	case '?':
	default:
	  std::cout << argv[0] << ": invalid option -- " << optopt << std::endl;
	  usage();
	  exit(EXIT_FAILURE);
	}
    }

  argc -= optind;
  argv += optind;

  if ( vflag )
    {
      version();
      exit(EXIT_SUCCESS);
    }

  MemoryInformation mi(factor);
  std::cout << mi << std::endl;
  
  return EXIT_SUCCESS;
}

void usage()
{
  std::cout << "usage: free [-b|-k|-m|-g] [-t] [-v]\n" 
    "\t-b,-k,-m,-g show output in bytes, KB, MB, or GB\n"
    "\t-t display logical summary for RAM\n"
    "\t-v display version information and exit" << std::endl;
}

void version()
{
  static const char *v = "v.0.0.1";
  std::cerr << v << "\nBuilt " << __DATE__ << " " << __TIME__ << std::endl;
}
