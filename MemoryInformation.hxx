#ifndef MEMORY_INFORMATION_HXX
#define MEMORY_INFORMATION_HXX

#include <cstddef>
#include <cctype>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/sysctl.h>
#include "BinaryPrefix.hxx"

class MemoryInformation
{
  friend std::ostream& operator<<(std::ostream &, const MemoryInformation &);
public:
  MemoryInformation(const size_t = BinaryPrefix::KB);
  ~MemoryInformation();
  void setScale(const size_t);
  size_t getScale() const;
  size_t getPhysicalMemory() const;
  size_t getVirtualMemory() const;
  size_t getPageSize() const;
  size_t getPhysicalPages() const;
  size_t getFreeMemory() const;
  size_t getUsedMemory() const;
  size_t getAvailableMemory() const;
  size_t getActiveMemory() const;
  size_t getInactiveMemory() const;
  size_t getBufferedMemory() const;
  size_t getCachedMemory() const;
  size_t getSwapMemory() const;
  size_t getFreeSwapMemory() const;
  size_t getUsedSwapMemory() const;
  size_t getSharedMemory() const;
  size_t getLockedMemory() const;
protected:
  std::string getMatchedString(const std::string &, const std::string &) const;
  size_t getMultiplier(std::string &) const;
  bool compareCaseInsensitive(std::string, std::string) const;
private:
  size_t scale;
};

#endif
