#ifndef FILE_SIZE
#define FILE_SIZE

#include <cstddef>

class BinaryPrefix
{
public:
  static const size_t KB = 1024;
  static const size_t MB = 1048576;
  static const size_t GB = 1073741824;
  static const size_t TB = 1099511627776;
};

#endif
