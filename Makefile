# Compiler
GXX := g++
GXXFLAG := -std=c++11 -Wall
MAIN := main
CXX := .cxx
HXX := .hxx
OBJ := .o
OBJ_LIST := MemoryInformation$(OBJ) BinaryPrefix$(OBJ)
LIB_LIST :=
.PHONY: all
all: $(MAIN)
	@ echo Build complete
$(MAIN): $(MAIN)$(OBJ) $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(MAIN)$(OBJ) $(OBJ_LIST) -o $(MAIN) $(LIB_LIST)
$(MAIN)$(OBJ): $(MAIN)$(CXX) $(MAIN)$(HXX)
	$(GXX) $(GXXFLAG) -c $(MAIN)$(CXX) -o $(MAIN)$(OBJ)
MemoryInformation$(OBJ): MemoryInformation$(CXX) MemoryInformation$(HXX)
	$(GXX) $(GXXFLAG) -c MemoryInformation$(CXX) -o MemoryInformation$(OBJ)
BinaryPrefix$(OBJ): BinaryPrefix$(CXX) BinaryPrefix$(HXX)
	$(GXX) $(GXXFLAG) -c BinaryPrefix$(CXX) -o BinaryPrefix$(OBJ)
.PHONY: clean check help
clean:
	rm -rf $(MAIN) *$(OBJ) *.out *~
	@ echo Clean complete
help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  all            build all targets (default)"
	@echo "  help           to print his output message"
	@echo "  clean          to remove tmp files"
